---
title: Business Plan - AllSafe Security
author: Cédric Martinez, Cédric Kreis et Cédric Tenthorey
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{graphicx}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \ \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ CMZ, CKS et CTY}
---
\let\emph\textit
\centering
![Logo](Figures\I-GO-CMZCKSCTY-Logo.png)
\newpage

\raggedright
\tableofcontents
\newpage
\justify


# AllSafe Security

## Présentation

Notre entreprise se nomme "AllSafe Security". Nous avons décidé, en 2017, de fonder une entreprise spécialisée dans le domaine de la sécurité informatique. Nous proposons à nos clients un large choix de services et de produits qui sont énumérés ci-dessous :

### Services

L'entreprise fournit plusieurs services aux clients.

- Pentesting
- Audit de sécurité
- Support utilisateurs
- Formation sur la sécurité

### Produits

 L'entreprise propose deux types de produits aux clients.

- Vente de solution d'antivirus Kaspersky
- Vente de la solution de monitoring et de gestion des évènements Allien Vault

## Membres

| Nom | Prénom | Adresse E-mail |
| :------------- | :------------- | :------------- |
| Kreis | Cédric | cedric.kreis@cpnv.ch |
| Martinez | Cédric | cedric.martinez@cpnv.ch |
| Tenthorey | Cédric | cedric.tenthorey@cpnv.ch |

Les CVs sont disponibles en annexe.

## Organisation

L'entreprise est organisée de manière verticale avec un chef et deux employés.

### Chef

Cédric Kreis       : Responsable des finances, informaticien et technicien réseau

### Employés

Cédric Martinez     : Informaticien, technicien et spécialiste en sécurité

Cédric Tenthorey    : Informaticien, technicien système et commercial

\newpage

## Organigramme

Vous trouverez ci-dessous l'organigramme de l'entreprise.

![Organigramme](Figures\I-GO-CMZCKSCTY-Organigramme.png)

\newpage

## Localisation

Nous avons décidé d'implémenter notre entreprise à Yverdon-les-Bains dans le canton de Vaud. Plus précisément, notre entreprise sera située à la Rue de Neuchâtel 16. Nous avons choisi cet emplacement car il est non loin du centre-ville et de la gare. De plus, cela en fait un emplacement bien situé et cela nous permet d'avoir une bonne visibilité. Pour finir, les locaux sont de bonnes dimensions et le loyer n'est pas trop onéreux pour notre petite entreprise.

Vous trouverez ci-dessous la capture du prix du local lors des recherches.

![Loyer](Figures\I-GO-CMZCKSCTY-PrixLocal.PNG)

La figure ci-dessous illustre l'emplacement sur une carte du local.

![Emplacement](Figures\I-GO-CMZCKSCTY-EmplacementLocal.PNG)

\newpage

# Étude de marché

## Politique des 4 P

La politique des 4 P consiste à répondre à des questions sur 4 thèmes qui sont du produit, du prix, du lieu, et de la publicité (Product, Price, Place, Promotion d'où le nom de Politique des 4 P) qui permettent l'identification des produits et des services d'une entreprise afin de mieux les comprendre.

### Product

**Quelle est la durée de vie du produit ?**

L'antivirus Kaspersky est renouvelé tous les ans par Kaspersky et nécessite un abonnement à renouveler.

Le SIEM AlienVault USM est mis à jour tous les 6 mois et un abonnement est nécessaire pour avoir les mises à jour.

Les formations fournies aux utilisateurs peuvent être certifiées sur une durée entre 6 mois et 5 ans selon la formation ce qui incite les clients à refaire des formations chez AllSafeSecurity.

Le pentesting et l'audit de sécurité sont des services onéreux qui sont recommandés tous les ans mais qui devraient être effectués périodiquement.

Le support aux utilisateurs se fait au cas par cas à la demande.

**Est-ce économiquement viable ?**

Avec une marge de 10 % par produit, la vente d'antivirus Kaspersky et de SIEM AlienVault permet d'engranger des bénéfices.

En tenant compte des frais de déplacement des techniciens, les formations, l'audit de sécurité, le pentesting et le support sont viables mais pourraient être proposés à perte lors des trois premiers mois afin d'avoir des clients.

**Quelles sont les fonctionnalités du produit ?**

L'antivirus Kaspersky permet de détecter les menaces avant qu'elles n'infectent le poste client et permet d'exécuter les fichiers suspects dans un bac à sable sécurisé afin d'évaluer son comportement.

Le SIEM AlienVault USM analyse le comportement des équipements à l'aides des trames réseaux redirigées vers lui. Il permet de détecter des comportements suspects et de trouver des équipements cachés et potentiellement infectés par des chevaux de Troie par exemple.

Les formations sur la sécurité qui seront proposées aux clients seront basées sur les Best Pratices du réseau Cybersecurity Alliance Ecosystem et peuvent être certifiées grâce au partenariat avec Kudelski Security qui propose des certifications OSINT, CISM, CBCI, GDPR, CISSP, ISO 27001 et 27005.

Le pentesting permet de détecter les failles de sécurité d'une infrastructure en simulant un attaquant externe qui essaierait d'accéder de manière non autorisée aux données d'entreprise et d'effectuer des actions malveillantes.

L'audit de sécurité a pour but de dresser une liste des failles de sécurité et des actions nécessaires afin de les corriger que ce soit au niveau technique mais aussi organisationnel.

Le support aux utilisateurs permet de résoudre les incidents à la demande avec un contrat de niveau de service (SLA) signé avec le client.

**Quel est son principal avantage ?**

L'antivirus Kaspersky est leader sur le marché des antivirus et a gagné de nombreux prix pour avoir réussi à détecter des virus cachés que d'autres antivirus n'ont pas réussi à détecter.

Le SIEM AlienVault USM est un produit reconnu sur le marché ayant une édition Open-Source et un accès à des schémas de comportements malveillants dans les éditions payant.

Les formations sur la sécurité sont organisées et certifiées en partenariat avec Kudelski Security une entreprise ayant une excellente réputation en matière de sécurisation des infrastructures bancaires notamment.

Le pentesting et les audits de sécurités effectués par les techniciens suivent les Best Pratices vues dans les certifications CISM et CISSP qui sont reconnues dans le monde entier, la formation des techniciens aux dernières menaces de sécurité permet de détecter les failles les plus vicieuses.

Le support des utilisateurs fourni par les techniciens est facilité par leur expérience durant leurs emplois antérieurs et la qualité des formations qu'ils ont reçus.

**Quelle est la gamme du produit ?**

* L'antivirus Kaspersky Total Security est un produit moyen de gamme destiné aux petites et moyennes entreprises.
* Le SIEM AlienVault USM est un produit moyen/haut de gamme destinées aux moyennes entreprises.
* Les formations à la sécurité sont des des services moyens de gamme destinées à des petites, moyennes et grandes entreprises.
* Le pentesting est un service haut de gamme destinés aux moyennes et grandes entreprises.
* L'audit de sécurité est un service moyen de gamme destiné à tout type d'entreprise.
* Le support aux utilisateurs est un service moyen de gamme qui convient aux particuliers comme aux entreprises.

**Est-ce un produit de masse, un produit moyen de gamme ou un produit de luxe ?**

* L'antivirus Kaspersky Total Security est un produit de masse puisqu’il est destiné aux particuliers comme aux entreprises.
* Le SIEM AlienVault USM est un produit de luxe destinées aux moyennes entreprises.
* Les formations à la sécurité sont des services moyens de gamme destinées à des petites, moyennes et grandes entreprises.
* Le pentesting est un service de luxe destinés aux moyennes et grandes entreprises à cause de son coût notamment.
* L'audit de sécurité est un service moyen de gamme destiné à tout type d'entreprise.
* Le support aux utilisateurs est un service de masse qui convient aux particuliers comme aux entreprises.

**Quel est l’apport de la marque au produit ?**

La marque AllSafeSecurity est indépendante, composée de techniciens ES en Informatique bien que peu connue ce qui donne une image de qualité aux produits dans un domaine où la méfiance des consommateurs est souvent de mise.

### Price

**Quel est le coût de production du produit ?**

Les antivirus et les SIEM sont achetés à Kaspersky et à AlieVault, leur coût est celui de leur achat qui peut être acheté au prix de vente ou à un prix préférentiel de revendeur selon le contrat qui sera signé avec ces entreprises. L'antivirus Kaspersky sera revendu au prix de 80 CHF et l'AlienVault USm édition Essentials au prix soit 800 CHF.

Les formations sur la sécurité nécessitent la location d'une salle de conférence ce qui se fait parfois à la journée, le paiement des honoraires d'un technicien estimé à 150 CHF par heure et le coût de la formation préalable du technicien qui peut se situer entre 5000 et 10000 CHF par certification sans compter le temps nécessaire pour étudier la certification et passer l'examen.

Le pentesting et l'audit de sécurité nécessitent l'achat préalable d'un laptop, le déplacement du technicien (1 CHF le kilomètre) ainsi que le tarif horaire de celui-ci (150 CHF de l'heure).

Le support aux utilisateurs peut se faire de préférence à distance ce qui nécessite une connexion Internet, la connexion choisie est une connexion chez Fiber7 à 777 CHF par an et un laptop par technicien ainsi que le tarif horaire du technicien 150 CHF de l'heure. Si cela n'est pas possible, il n'est plus nécessaire de disposer d'une connexion Internet mais les frais de déplacement du technicien sur site s'appliquent.

**Est-ce qu’une alternative meilleure marché est-elle possible ?**

Pour l'antivirus Kaspersky et le SIEM AlienVault, il serait possible de tenter de négocier un meilleur prix lors de la création du contrat avec les entreprises mais il n'est pas certain que cela soit un succès et AllSafeSecurity risquerait de ne pas pouvoir vendre ces produits.

Pour les formations, il est possible d'éviter la location d'une salle en utilisant le local à Yverdon de l'entreprise mais cela nécessiterait l'achat de chaises et d'un beamer, il n'est pas certain que cela soit plus avantageux que la location d'une salle.

Pour le pentesting et l'audit de sécurité, il est éventuellement possible d'éviter le déplacement chez le client en effectuant l'analyse depuis le siège de AllSafeSecurity mais cela pourrait diminuer la qualité de l'audit et ne pas produire les résultats attendus.

Concernant le support aux utilisateurs, parfois il est possible d'éviter de faire se déplacer un technicien sur place selon la demande ce qui réduit légèrement les coûts mais cela n'est pas toujours possible, il n'y a donc pas toujours d'alternative meilleur marché possible.

**Quel est le prix que les clients acceptent de payer ?**

Le sondage effectué a permis de démontrer que la somme concernant la sécurité que les clients potentiels seraient prêt à payer était comprise entre 0 CHF et 10000 CHF.

**Quelle est la valeur du produit selon les clients ?**

Selon le sondage, les clients accordent une grande importance à la sécurité en général. L'antivirus semble avoir une plus grande importance par rapport au SIEM qui est souvent inconnu des clients bien que les deux produits soient complémentaires.

**Quel est le prix minimal possible?**

Pour l'antivirus Kaspersky et le SIEM AlienVault le prix minimal possible est le prix d'achat à l'entreprise augmenté d'une marge de 10%.

Concernant les formations données aux utilisateurs, le prix minimal serait le tarif horaire du technicien soit 150 CHf de l'heure majoré de 10%.

**Est-ce que le marché est essentiellement composé de clientèle haut de gamme ?**

Après un sondage, il a été constaté que le marché est essentiellement composé de clientèle bas à moyen de gamme ce qui correspond à l'estimation initiale.

**Est-ce qu’un prix élevé constitue un avantage?**

D'après le sondage effectué, un prix élevé ne semble pas être un avantage puisque les clients désirent payer le moment le moins élevé possible.

**Est-ce que la quantité de produits vendus est déterminant ?**

La quantité d'antivirus et de SIEM vendus est déterminant pour la viabilité économique de l'entreprise. Il est donc primordial de conclure un accord avec un client avant de faire 'acquisition auprès de l'entreprise du produit.

**Est-ce qu’une vente à perte est-elle autorisée ?**

Lors des recherches, aucune indication n'a été trouvée permettant de démontrer qu'il ne serait pas autorisé de vendre les produits à perte.

**Est-il possible de faire une entente entre les concurrents ?**

La loi suisse interdit les accords sur les prix.

**Quelles sont les conditions de garantie ?**

Les produits sont soumis à une garantie valable durant la durée du contrat négocié avec le client.

### Place

**Quel est le canal de distribution principal ?**

Les produits seront principalement distribués via le local à Yverdon, la création d'un site Internet est à envisager.

**Est-ce que la diversité des distributeurs est importante ?**

Chaque produit proposé aux clients est produit par une entreprise différente, l'entreprise AllSafeSecurity sert de distributeur unique mais les antivirus Kaspersky et le SIEM AlienVault USM sont redistribués par de nombreux distributeurs à travers le monde.

**Est-ce que l’image de marque est-elle importante ?**

La vente des produits et des services est basée sur la confiance des clients envers AllSafeSecurity, il est donc primordial que l'entreprise ait une bonne image de marque et une excellente réputation.

**Est-ce que le produit ne peut pas être distribué partout ?**

L'entreprise AlienVault étant basée en Suisse et que les produits sont fournis avec du support qui prévoit la venue d'un technicien sur place, il semble difficile d'envisager la distribution du produit au-delà d'un périmètre de 200 kilomètres durant les 5 premières années mais ce rayon est mené à s'élargir au fur et à mesure de la croissance de l'entreprise.

**Est-ce que la vente directe est-elle une priorité ?**

Puisque l'entreprise fourni du support aux clients pour les produits et les services qu'elle fournit, il est nécessaire de vendre les produits directement sous peine de pas pouvoir assurer la qualité de service proposée au client.


**Est-ce qu’il est important d’utiliser un réseau de grande distribution pour le produit ?**

Le produit est distribué via le local situé à Yverdon et utiliser un réseau de grande distribution mettrait notre entreprise en concurrence avec Kaspersky par exemple qui vend leurs produits dans certains réseaux de grande distribution c'est pourquoi cette possibilité a été écartée.

**Est-ce que l’indépendance du réseau de distribution est-elle importante ?**

Sachant que l'entreprise crée un contact privilégié avec le client, il est important de rester maître de la chaîne de distribution.

**Où est-ce que le produit est-il demandé ?**

Selon un article du temps, une entreprise sur 4 en suisse romande a déjà subi une cyberattaque, ce qui crée une forte demande dans la région lémanique.

**Est-il possible de vendre le produit aux endroits où il est demandé ?**

La région lémanique est située dans un périmètre ne dépassant pas les 250 kilomètres du local d'Yverdon, c'est pourquoi il semble possible de vendre les produits et les services de AllSafe Security aux clients potentiels.

**Est-il important de faire une démonstration ?**

L'entreprise AllSafeSecurity étant nouvelle sur le marché il est important de faire bonne impession aux clients et démontrer les compétences des techniciens dans leur choix de l'antivirus et du SIEM. Un portfolio ainsi que des démonstrations en public dans le local à Yverdon pourraient être envisagés.

**Quel est l’aspect à privilégier par rapport à la présentation du produit ?**

La qualité de l'antivirus et du SIEM doivent être mis en avant afin que les clients puissent avoir confiance et être incités à les acheter.

**Quel est l’atout principal du produit ?**

L'antivirus Kaspersky est leader sur le marché des antivirus et a gagné de nombreux prix pour avoir réussi à détecter des virus cachés que d'autres antivirus n'ont pas réussi à détecter.

Le SIEM AlienVault USM est un produit reconnu sur le marché ayant une édition Open-Source et un accès à des schémas de comportements malveillants dans les éditions payant.

Les formations sur la sécurité sont organisées et certifiées en partenariat avec Kudelski Security une entreprise ayant une excellente réputation en matière de sécurisation des infrastructures bancaires notamment.

Le pentesting et les audits de sécurités effectués par les techniciens suivent les Best Pratices vues dans les certifications CISM et CISSP qui sont reconnues dans le monde entier, la formation des techniciens aux dernières menaces de sécurité permet de détecter les failles les plus vicieuses.

Le support des utilisateurs fourni par les techniciens est facilité par leur expérience durant leurs emplois antérieurs et la qualité des formations qu'ils ont reçus.

**Quelle est la manière désirée de vente du produit ?**

Les produits et les services devraient être vendus via un contrat avec l'entreprise cliente.

### Promotion

**Est-ce qu’il existe un quotidien local qui pourrait faire la promotion du produit ?**

La commune d'Yverdon dispose d'un quotidien nommé La Région dans lequel la publication d'annonces est possible. ^[Voir https://www.laregion.ch/wp-content/uploads/plaquette-laregion-2017.pdf]

**Quel est le public cible du produit ?**

Les produits et les services de AllSafe Security  visent les petites et moyennes entreprises situées en suisse romande.

**Quels sont les médias utilisés par le public cible ?**

Internet est le principal média utilisé par ces entreprises.

**Est-il possible de demander à des journalistes d’effectuer un test du produit ?**

Oui, c'est une possibilité qui permettrait de mettre en avant les compétences et le sérieux de l'entreprise.

**Est-il possible de faire des revues du produit à des vidéastes en ligne ?**

Sachant que le public cible de AllSafe Security ne consomme que peu de vidéos en ligne, il semble plus adapté de prioriser les tests effectués par des journalistes indépendants.

**Est-il pertinent d’effectuer une enquête de satisfaction ?**

Une enquête de satisfaction envoyées à tous les clients de l'entreprise à une intervalle de 6 mois semble être un choix judicieux.

**Quels sont les points à aborder lors d’une enquête concernant les désirs du consommateur ?**

Il faudrait aborder la question des produits dont il pourrait avoir besoin ainsi que sa satisfaction par rapport à l'offre proposée sans oublier les éventuelles suggestions qu'il pourrait apporter à AllSafe Security.

**Quels sont les évènements publics permettant une grande visibilité du produit ?**

Afin de renforcer l'image d'une entreprise compétente, il faudrait participer à des séminaires sur la sécurité informatique comme The International Information Security Summit à Londres par exemple ou à des compétitions de hacking comme Insomi’Hack à Genève ce qui démontrerait les capacités des techniciens de l'entreprise.

### Marketing Mix

#### Analyse des résultats

\mbox{}

La politique des 4 P a permis de démontrer que les produits sont adaptés au marché des petites entreprises mais qu'il était important de développer une reconnaissance de la part des consommateurs afin qu'ils fassent l'acquisition d'un produit comme l'AlienVault USM ce qui est dû à la position de nouveaux arrivants sur le marché de l'entreprise AllSafeSecurity.

\newpage

## Analyse SWOT

Une analyse SWOT de l'entreprise a été effectuée en prenant tous les facteurs de l'entreprise.

### Forces

L'entreprise disposes de nombreuses forces dont notamment les compétences techniques des techniciens grâce à leur formation de Technicien ES en informatique spécialisation système d'excellente qualité et très réputée.

### Faiblesses

Du point de vue des faiblesses, l'entreprise pourrait être confrontées à de nouvelles menaces évoluant extrêmement vite et nécessitant une actualisation continue des connaissances des techniciens sur les nouvelles failles de sécurité.

### Opportunités

L'entreprise peut profiter de nombreuses opportunités dont par exemple l'infrastructure peu sécurisée des clients, les entreprises ayant déjà subi des attaques et désirant se protéger ou encore de la médiatisation des cyberattaques provoquant une peur de celles-ci chez les entreprises ainsi qu'un désir de formation des utilisateurs.

### Menaces

Parmi les menaces identifiées, la confiance des clients potentiels envers les concurrents représente la menace la plus inquiétante ainsi que les clients disposant d'une équipe interne dédiée à la sécurité pouvant effectuer la mise en place des patchs.

![Analyse SWOT](Figures\I-GO-CMZCKSCTY-AnalyseSWOT.PNG)

\newpage

## Concurrents

Afin de bien connaître le marché actuel, nous avons effectué une recherche sur les sociétés concurrentes. Cela va nous permettre de nous faire une idée des prix du marché ainsi que des produits et services proposés.

### Scrt SA

Cette société est basée en Suisse et en France. Le HeadQuarter sont à Préverenges et ils possèdent également une filiale à Berne. Elle est exclusivement spécialisée dans la sécurité des systèmes d'information. Cette entreprise travaille avec des clients de toutes tailles comme des PME et des grandes sociétés européennes.

#### Produits et services

\mbox{}

- Pentesting
- Audit de code source
- Analyse de l'infrastructure
- Reverse engineering
- Formations (Fortinet, Sécurisation Linux et de cloud, PKI Microsoft)

#### Prix

\mbox{}

Les prix ne sont pas communiqués sur le site internet de Scrt SA.

### Kudelski Security

Société basée à Cheseaux-sur-Lausanne, spécialisée dans la sécurité bancaire et les audits de sécurité. Elle travaille avec tout type d'organisation, petite ou grande. ^[Voir https://www.kudelskisecurity.com/about-us]

#### Services

\mbox{}

L'entreprise fourni une large palette de services dont notamment :  

- Conseil aux utilisateurs
- Développement de solutions sécurisées
- Gestion de la sécurité chez les Clients
- Accès au réseau d'expertise Cybersecurity Alliance Ecosystem
- Formations (OSINT, CISM, CBCI, GDPR, CISSP, ISO 27001 et 27005)

#### Prix

\mbox{}

Il est possible de faire un devis par l'entreprise Kudelki Security. Un formulaire est à disposition sur le site internet de l'entreprise.

### Net4all

Société basée à Ecublens dans le canton de Vaud mais également active en France où elle y possède son siège social. Cette société est spécialisée dans l'hébergement web ainsi que dans le Cloud. De plus, cette entreprise propose aussi des services concernant la sécurité informatique comme des audits de sécurité et des tests d'intrusions. Elle travaille avec tout type de client, comme des particuilers, des PME ou de plus grandes entreprises.

#### Produits et services

\mbox{}

- Audit de sécurité informatique
- Tests d'intrusion interne et externe
- Scan de site web (hack protect)

#### Prix

\mbox{}

Les prix ne sont pas communiqués sur le site internet de Net4all.

## Partenaires

### Kaspersky

L'entreprise Kaspersky fait un partenaire idéal dans notre business model. En effet, être partenaire avec cette entreprise permet de disposer de remises sur leurs produits et de leur expertise en contrepartie l'entreprise s'engage é vendre que des produits anti-virus Kaspersky.

### Kudelski Security

Un partenariat est envisagé avec l'entreprise Kudelski Security notamment grâce à leur expertise dans le domaine de la sécurité informatique et leur réseau d'experts Cybersecurity Alliance Ecosystem qui permet à AllSafe Security de disposer d'une base solide et des dernières informations sur les menaces de sécurité.

## Sondages

### Méthode choisie

Nous avons envoyé, afin de récolter des informations pour notre futur entreprise, un formulaire Google Forms contenant un sondage avec diverses questions qui nous seront utiles afin d'affiner nos futurs services. Ce questionnaire a été envoyé à un public représentatif d'employés et de chefs d'équipe informatique.

\newpage

### Résultat des sondages

#### Sécurité des infrastructures informatique  

\mbox{}

Après envoi du sondage, les résultats ont été analysés. Après analyse du sondage, Il est possible de constater que les personnes interrogées travaillent dans des secteurs où les systèmes informatiques ont une très grande importance dans leur business. De plus, ils estiment que la sécurité est une chose importante voir très importante dans leur entreprise. De ce fait, cela démontre qu'un large éventail d’entreprises qui pourraient être potentiellement intéressées par les services de AllSafeSecurity.  

![Importance des systèmes informatique](Figures\I-GO-CMZCKSCTY-SondageQuestion1.PNG)  
![Importance de la sécurité informatique sur l'infrastructure](Figures\I-GO-CMZCKSCTY-SondageQuestion2.PNG)

\newpage  

#### Logiciel de surveillance réseau + centralisation des logs

\mbox{}

Après analyse, il est possible de constater que les personnes interrogées ne possèdent en général pas de logiciels qui regroupent les événements de sécurité sur leur infrastructure (69.2%). De ce fait, ce pourcentage de personnes est potentiellement un client pour l’achat de notre produit AlienVault USM.

![SIEM](Figures\I-GO-CMZCKSCTY-SondageQuestion3.PNG)  

#### Audit

\mbox{}

Il est possible de constater que presque la moitié (46.2%) des sondés, ont déjà effectués un audit de leur infrastructure.  Mais que 46.2% seraient intéressés à refaire un audit. Cela permettrait de leur offrir les services d'audit de sécurité afin d’effectuer cette tâche.

![Audit de sécurité](Figures\I-GO-CMZCKSCTY-SondageQuestion4.PNG)  
![Audit de sécurité](Figures\I-GO-CMZCKSCTY-SondageQuestion5.PNG)  

#### Formation utilisateurs

\mbox{}

Il est possible de constater que 92.2% des personnes interrogées n’ont jamais formé leurs utilisateurs sur le point de la sécurité informatique. Ces entreprises pourraient potentiellement être de futurs clients afin de leur vendre les services de formation sur la sécurité informatique.

![Formation utilisateur](Figures\I-GO-CMZCKSCTY-SondageQuestion6.PNG)  

\newpage

#### Antivirus

\mbox{}

Il est possible de constater que 84.6% des sondés possèdent déjà un antivirus. Cela signifie que la vente de l’antivirus Kaspersky ne seraient qu’une petite partie du business d'AllSafeSecurity et qu’il ne faut pas s’attendre à une grande rentrée d’argent pour la vente de cette solution.  

![Antivirus](Figures\I-GO-CMZCKSCTY-SondageQuestion7.PNG)  

\newpage

## Coûts

Plusieurs coûts doivent être pris en compte par l'entreprise dont:

* Achat des antivirus et des SIEM
* Formations sur de nouveaux produits
* Séminaires sur la Sécurité
* Loyer
* Véhicule
* Charges fixes
* Mobilier de bureau

Les achats se feront via un contrat avec Kaspersky et un contrat avec AlienVault afin de revendre leurs produits.

Le prix du loyer du local est de 543 CHF par an charges comprises ainsi que l'assurance incendie obligatoire dans le canton de Vaud ECA d'une valeur annuelle de 30 CHF.

Un véhicule sera acheté afin de permettre le déplacement des employés chez les clients, le véhicule choisi est une Opel Insignia TS de 2009 au prix de 8800 CHF.
Ce véhicule a une taxe sur la circulation annuelle estimée de 708,80 CHF. Pour faire circuler le véhicule, des plaques de circulation valant 60 CHF sont nécessaires.
Ainsi qu'une assurance ayant une prime de 2'123,80 CHF par an. Le véhicule ayant une motorisation à essence, une consommation maximale de 13,7 litres au 100 kilomètres environ ainsi qu'un réservoir de 70 litres, le coût de l'essence est estimé à 5'376 CHF par an avec un prix au litre de 1.60 CHF et un plein complet par semaine ce qui procure une autonomie hebdomadaire de 511 kilomètres environ.

Ce qui porte le prix de l'investissement initial à 8'860 CHF avec un coût annuel récurrent de 8'268,60 CHF.

Chaque employé disposera d'un bureau Bekant d'une valeur de 299 CHF ainsi que de 50 stylos Office World Scriva Büro de couleur bleue à 9.90 CHF et de 10 surligneurs A-series Highlighter jaunes à 9.90 CHF.

Ce qui représente un total de 956,40 CHF d'investissement initial pour le mobilier de bureau et la papeterie ainsi qu'un coût annuel de 19.8 CHF.

Les employés auront à disposition chacun un laptop Lenovo Thinkpad E570 d'une valeur de 799 CHF.
Ainsi que d'une station de docking Lenovo USB-C de 90W par laptop d'une valeur de 195 CHF soit un total de 2982 CHF pour les équipements informatiques.

Une imprimante HPE M477fnw d'une valeur de 376 CHF sera à disposition des employés. Cette imprimante nécessitera l'achat d'un toner de chaque couleur permettant l'impression de 6500 pages tous les 6 mois pour 659 CHF. Afin de pouvoir imprimer des blocs de papier pour imprimante au format A4 sera acheté à 5.80 CHF le bloc de 500 feuilles.

Cela représente un total de 381,8 CHF pour l'investissement initial pour l'imprimante et un coût récurrent annuel de 1'468,80 CHF pour le toner et le papier à raison de 13000 pages imprimées par an.

\newpage

## Sources de revenus

L'entreprise dispose de plusieurs sources de revenus dont notamment :  

* Marge sur le prix de vente des SIEM Alientvault USM
* Consulting
* Formations données aux clients
* Support après-vente
* Pentesting
* Marge sur le prix de vente des antivirus

### Prix de vente des SIEM Alientvault USM

Le prix de vente est de 800.- et le prix d'achat est de 650.-. Nous gagnons 150.- qui sont les frais de la mise en place du produit et du support en complément. Nous avons estimé que nous allions vendre 10 solutions Allien AlientVault durant la première année.

### Consulting

Le tarif de consulting est de 150.-/heure et par employé. Nous avons estimé que les six premiers mois nous ferions 20 heures de consulting et ensuite nous passerons à 120 heures par mois.

### Formations données aux clients

Le tarif pour les séminaires et les formations des clients s'élève à 500 CHF. Ce tarif a été décidé par les trois fondateurs de l'entreprise. Nous avons estimé que nous allions effectuer 15 formations durant l'année. Nous avons également pris en compte que nous n'en ferions pas les 3 premiers mois.

### Support

Le prix du support est également de 150 CHF par heure comme pour le consulting. Nous avons calculé que nous ferrions 20h de support les 4 premiers mois. Ensuite, nous avons estimé à 60h pour les 3 mois suivants. Finalement, nous avons éstimé 120 heures de support par mois les 6 derniers mois de l'année.

### Pentesting

Le pentesting est l'activité principale de l'entreprise. Cette activité rapporte 1200 CHF. Ce tarif a été fixé en fonction du prix des concurrents. Nous avons estimé que nous allions effectuer 4 pentesting durant la première année.

### Prix de vente des antivirus

La marge de vente sur les antivirus est de **10%**. Ce choix est porté sur le faite que nous conseillons nos clients et effectuons l'installation du produit. La somme totale d'antivirus vendus sur l'année a été estimée à 10.

### Petites entreprises

Nos principaux clients seront des petites à moyennes entreprises situées en suisse romande et ayant probablement déjà subi un incident de sécurité ce qui les aura alertés sur la nécessité de se tourner vers un prestataire de services comme notre entreprise afin de les protéger contre les cyberattaques.

\newpage

# Offre

## Services

### Consulting

#### Pentesting

Le service de pentesting ou test d'intrusion sont proposés aux clients, le but de ce service est de permettre la détection les failles de sécurité d'une infrastructure en simulant un attaquant externe qui essaierait d'accéder de manière non autorisée aux données d'entreprise et d'effectuer des actions malveillantes. Un technicien utilisera un laptop muni de la distribution Kali Linux qui est un système d'exploitation centré sur les tests de sécurité et tentera de se connecter aux divers services de l'entreprise en utilisant des schémas d'attaque connus. Il rendra un rapport avec un tableau de résultats au client avec les failles détectées.

\mbox{}

#### Audits de sécurité

Les clients sont incités à effectuer tout d'abord un audit de sécurité qui a pour but de dresser une liste des failles de sécurité et des actions nécessaires dans un rapport afin de les corriger que ce soit au niveau technique mais aussi organisationnel. Le rapport est soumi à un accord de non-divulgation établi dans le contrat de niveau de service avec le client. Le rapport peut déboucher sur une formation des utilisateurs à la sécurité ou une refonte des process de l'entreprise et de la gestion de son infrastructure.

\mbox{}

### Formations

#### Formations à la sécurité

Les formations sur la sécurité sont proposées aux clients sont basées sur les recommendations réseau Cybersecurity Alliance Ecosystem via le partenariat avec Kudelski Secuirty et des certifications sont proposées notammant les certifications OSINT, CISM, CBCI, GDPR, CISSP, ISO 27001 et 27005. Afin de pouvoir mieux cibler les formations, un entretien avec le client est proposé initialement avec s'il le désire un audit de sécurité afin que les formations ciblent les points faibles de son entreprise.

\mbox{}

### Support

#### Support aux utilisateurs

\mbox{}

Le support sera principalement basé sur les utilisateurs, le but étant de corriger d'éventuelles failles de sécurité, puis inciter les entreprises à faire l'acquisition de nos formations.

L'outil de ticketing GLPI gratuit et libre sera utilisé en interne afin de gérer les tickets des clients, il sera implémenté sur un serveur dans le local de l'entreprise.

### Vente d'antivirus

#### Kaspersky

\mbox{}

Grâce au partenariat avec Kaspersky, il sera possible de fournir aux clients un anti-virus Kaspersky Total Security avec une marge de 10% avec probablement un rabais sur le prix de vente final.

### Vente de SIEM

#### AlienVault USM

\mbox{}

Le SIEM AlientVault USM dévelopé par AlienVault est un outil de gestion des événements et des informations de sécurité (Security Information and Event Management), son but est de permettre l'analyse proactive des fichiers de logs et détecter les schémas d'attaques afin de pouvoir obtenir un niveau supérieur de sécurité et de contrôle des événements dans une infrastructure informatique. ^[Voir https://en.wikipedia.org/wiki/Security_information_and_event_management]

L'édition essentials sera mise à disposition de clients n'ayant une petite infrastructure de moins de 100 employés, ce produit est vendu au prix de 650 USD par mois par AlienVault et sera revendu aux clients au prix de 800 CHF par mois en incluant le support. ^[Voir https://www.alienvault.com/pricing]

L'édition standard ne sera pas mise à disposition des clients à cause du prix trop élevé puisque vendu au prix de 1575 USD par mois par AlienVault et du manque de marché pour un tel produit car le prix de vente au client serait inintéressant. ^[Ibid.]

## Ressources

### Réseau d'expertise Cybersecurity Alliance Ecosystem

L'accès à la base de connaissance et à l'expertise du réseau Cybersecurity Alliance Ecosystem via Kudelski Security permet de fournir aux techniciens une base solide sur laquelle s'appuyer en cas de cyberattaque chez un client. ^[Voir https://www.kudelskisecurity.com/services]

La formation des clients sera ainsi améliorée grâce aux Best Pratices fournies par celle-ci.

## Distribution

Les services et les produits seront distribués via notre espace magasin situé dans le local à Yverdon.

Le support sera fait chez les clients ou à distance tant que possible afin d'éviter les déplacements des techniciens.

Des journalistes seront contactés afin de permettre de démontrer les compétences des techniciens.

\newpage

# Références

## Bibliographie

Aucun livre n'a été consulté lors de la rédaction du présent document.

## Webographie

Vous trouverez ci-dessous la liste des sites web consultés durant la rédaction du présent document.

https://en.wikipedia.org/wiki/Security_information_and_event_management, consulté le 30 janvier 2018

https://www.kudelskisecurity.com/services, consulté le 30 janvier 2018

https://www.kudelskisecurity.com/about-us/company, consulté le 30 janvier 2018

https://www.net4all.ch/qui-sommes-nous/, consulté le 30 janvier 2018

https://www.net4all.ch/architecture-de-cloud/, consulté le 30 janvier 2018

https://www.net4all.ch/securite-informatique/, consulté le 30 janvier 2018

https://www.alienvault.com/pricing, consulté le 13 février 2018

http://www.rsv.vd.ch/rsvsite/rsv_site/doc.pdf?docId=1299568&Pvigueur=&Padoption=&Pcurrent_version=9999&PetatDoc=vigueur&Pversion=&docType=reglement&page_format=A4_3&isRSV=true&isSJL=true&outformat=pdf&isModifiante=false, consulté le 26 février 2018

https://www.vd.ch/themes/mobilite/automobile/taxe-tarifs/taxe/voitures-ou-vehicules-de-lt-3500-kg/, consulté le 26 février 2018

https://fr.comparis.ch/autoversicherung/berechnen/result?inputguid=614cc700-028f-4f2c-83fd-d85859aea638, consulté le 26 février 2018

https://www.eca-vaud.ch/images/assurance/07_MEN_Brochure.pdf, consulté le 26 février 2018

https://www.digitec.ch/fr/s1/product/hp-410x-toner-m-c-y-bk-toners-5689066?productReferrer=5622692, consulté le 26 février 2018

https://www.laregion.ch/wp-content/uploads/plaquette-laregion-2017.pdf, consulté le 26 février 2018
