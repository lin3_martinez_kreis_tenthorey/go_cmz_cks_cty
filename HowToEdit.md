# Comment cloner la documentation?

Afin de modifier la documentation il est nécessaire d'installer git.

Puis, il faut installer Atom, les packages Pandoc, pandoc-convert, git-plus ainsi que le logiciel GitKraken puis se créer un compte sur GitKraken et lier son compte GitKraken à BitBucket.

Puis, il faut cliquer sur l'icone en forme de dossier en haut à gauche, puis cliquer sur le lien Open.

Il faut ensuite copier la ligne suivante dans le champ URL to repo.

```
https://bitbucket.org/lin3_martinez_kreis_tenthorey/go_martinez_kreis_tenthorey.git
```

Il faut ensuite sélectionner un dossier en locas sur son poste à l'aide du bouton Browse.

Puis, il faut cliquer sur le bouton Clone.

# Comment modifier le wiki?

Il faut ensuite modifer les fichiers .md à l'aide de Atom ou d'un autre editeur de texte situés dans le dossier dans lequel vous aurez cloné le wiki et sauvegarder.

# Confirmer les changements

Afin que les changements soient validés, il faut lancer GitKraken puis cliquer sur la ligne nommée WIP et cliquer sur le bouton Stage all changes ce qui va confirmer tous les changements pour le prochain push.

Il faut ensuite saisir un titre pour le changement dans le champ Summary ainsi qu'une description dans le champ Description.

Puis, il faut cliquer sur le bouton Stage files/changes to commit.

# Uploader les changements

Afin que les changements soient envoyé sur le git de bitbucket, il faut ouvrir GitKraken et cliquer sur le bouton Push.

Puis, il faut saisir ses identifiants BitBucket et cliquer sur Push.
